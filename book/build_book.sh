#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

pushd $toplevel
  pushd book
    for pysrc in $(ls *.py); do
      jupytext --sync $pysrc
    done
    # cleanup old build artifacts
    rm -rf _build ../public
    cp -a ../README.md intro.md
    cp -a ../INSTALL.md qsim_installation.md
  popd
 # build the jupyter book
 jupyter-book build book
 # move the build artifacts to the public folder
 rm -rf public
 mv book/_build/html public
 rsync -av book/images/ public/images
 chmod -R ugo+rX public
popd
